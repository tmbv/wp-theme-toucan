/*
 * Customizer.js to reload changes on Theme Customizer Preview asynchronously.
 *
 */

( function( $ ) {

	/* Default WordPress Customizer settings */
	wp.customize('blogname', function(value) {
		value.bind( function(to) {
			$('#logo .site-title').text( to );
		} );
	} );
	wp.customize('blogdescription', function(value) {
		value.bind(function(to) {
			$('#logo .site-description').text(to);
		} );
	} );
	wp.customize('header_color', function(value) {
		value.bind(function(to) {
			console.log('update header to ' + to)
			$('#header-wrap').css('background', to);
		});
	});
	wp.customize('heading_color', function(value) {
		console.log('heading color value!')
		value.bind(function(to) {
			console.log('heading color to ' + to)
			$('.page-title .post-title').css('color', to);
			$('.post-title a:link').css('color', to);
		});
	});

})(jQuery);


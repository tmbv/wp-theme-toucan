# Toucan Wordpress Theme

This is a fork of [Dukan Lite](http://odude.com/themes/dukan/) Wordpress theme, [version 1.15](https://themes.svn.wordpress.org/dukan-lite/1.15/).

# How to create a ZIP theme

```
git archive --format zip --prefix=toucan/ --output /path/to/toucan.zip master
```
